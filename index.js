Vue.component('todo-item', {
  props: ['todo'],
  template: '<li>{{todo.text}}</li>'
})

let app = new Vue({
  el: '#app',
  data: {
    team1Name: 'team 1',
    team1Score: 0,
    team2Name: 'team 2',
    team2Score: 0,
    showSettings: false,
    scoreIncreaseAmount: 1,
  },
  methods: {
    show: function () { this.showSettings = !this.showSettings; console.log(this.$refs.settings.innerText) },
    updateIncreaseAmount: function (e) { this.scoreIncreaseAmount = Number(e.target.value) },
    resetScores: function () { this.team1Score = 0; this.team2Score = 0; }
  }
})